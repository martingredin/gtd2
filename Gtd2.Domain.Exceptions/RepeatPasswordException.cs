﻿using System;

namespace Gtd2.Domain.Exceptions
{
	public class RepeatPasswordException: Exception
	{
		public RepeatPasswordException()
			: base("The password and repeat password aren't the same.")
		{ }
	}
}
