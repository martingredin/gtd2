﻿using System;

namespace Gtd2.Domain.Exceptions
{
	public class UserAlreadyExistsException : Exception
	{
		public UserAlreadyExistsException(string userName)
				:base(string.Format("A user with the name {0} already exsists, please choos anothe name.", userName))
		{ }
	}
}
