﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Domain.Exceptions
{
	public class NoSuchStateException : Exception
	{
		public NoSuchStateException(int id)
			: base(string.Format("There is no state with id {0}", id)) { }
	}
}
