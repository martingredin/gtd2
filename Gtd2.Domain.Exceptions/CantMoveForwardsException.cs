﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Domain.Exceptions
{
	public class CantMoveForwardsException : Exception
	{
		public CantMoveForwardsException()
			: base("You cant move forwards you are allready at the end.")
		{ }
	}
}
