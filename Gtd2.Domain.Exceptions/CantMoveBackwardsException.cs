﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Domain.Exceptions
{
	public class CantMoveBackwardsException : Exception
	{
		public CantMoveBackwardsException()
			: base("Youy cant move backwards you are allready at the beginning.")
		{ }
	}
}
