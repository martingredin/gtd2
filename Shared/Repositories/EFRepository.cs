﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Repositories
{
	public class EFRepository<T> : IRepository<T> where T : class
	{
		protected DbSet<T> dbSet;
		protected DbContext DataContext { get; private set; }

		public EFRepository(DbContext dataContext)
		{
			DataContext = dataContext;
			dbSet = DataContext.Set<T>();
		}

		public T GetById(int id)
		{
			return dbSet.Find(id);
		}

		public void Add(T entity)
		{
			dbSet.Add(entity);
		}

		public void Remove(T entity)
		{
			dbSet.Remove(entity);
		}

		public IEnumerable<T> List()
		{
			return dbSet.ToList<T>();
		}

		public void SaveChanges()
		{
			DataContext.SaveChanges();
		}
	}
}