﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontPageComponent } from './front/frontpage.component';
import { StateMgmtComponent } from './stateMgmt/statemgmt.component';
import { TaskMgmtComponent } from './taskMgmt/taskmgmt.component';

// Route Configuration
export const routes: Routes = [
	{ path: '', redirectTo: '/front', pathMatch: 'full' },
	{ path: 'front', component: FrontPageComponent },
	{ path: 'stateMgmt', component: StateMgmtComponent },
	{ path: 'taskMgmt', component: TaskMgmtComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);