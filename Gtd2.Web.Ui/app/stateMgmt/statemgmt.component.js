"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const task_service_1 = require('../taskMgmt/task.service');
const MoveTaskDto_1 = require('../MoveTaskDto');
let StateMgmtComponent = class StateMgmtComponent {
    constructor(taskService) {
        this.taskService = taskService;
        this.tasks = this.taskService.getTasks();
    }
    moveToNextState(taskId) {
        let moveTaskDto = new MoveTaskDto_1.MoveTaskDto();
        moveTaskDto.taskId = taskId;
        moveTaskDto.direction = 1;
        this.taskService.moveTask(moveTaskDto);
    }
    moveToPreviousState(taskId) {
        let moveTaskDto = new MoveTaskDto_1.MoveTaskDto();
        moveTaskDto.taskId = taskId;
        moveTaskDto.direction = 0;
        this.taskService.moveTask(moveTaskDto);
    }
};
__decorate([
    core_1.Input(), 
    __metadata('design:type', Array)
], StateMgmtComponent.prototype, "tasks", void 0);
StateMgmtComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'statemgmt.component.html',
        styleUrls: ['statemgmt.css'],
    }), 
    __metadata('design:paramtypes', [task_service_1.TaskService])
], StateMgmtComponent);
exports.StateMgmtComponent = StateMgmtComponent;
//# sourceMappingURL=statemgmt.component.js.map