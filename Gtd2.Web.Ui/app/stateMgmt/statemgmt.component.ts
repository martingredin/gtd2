﻿import { Component, Input } from '@angular/core';
import { TaskService } from '../taskMgmt/task.service';
import { TaskDto } from '../TaskDto';
import { MoveTaskDto } from '../MoveTaskDto';

@Component({
	moduleId: module.id,
	templateUrl: 'statemgmt.component.html',
	styleUrls: ['statemgmt.css'],
})
export class StateMgmtComponent {
	@Input() private tasks: TaskDto[];

	constructor(private taskService: TaskService) {
		this.tasks = this.taskService.getTasks();
	}

	moveToNextState(taskId: number) {
		let moveTaskDto = new MoveTaskDto();
		moveTaskDto.taskId = taskId;
		moveTaskDto.direction = 1;
		this.taskService.moveTask(moveTaskDto);
	}

	moveToPreviousState(taskId: number) {
		let moveTaskDto = new MoveTaskDto();
		moveTaskDto.taskId = taskId;
		moveTaskDto.direction = 0;
		this.taskService.moveTask(moveTaskDto);
	}
}