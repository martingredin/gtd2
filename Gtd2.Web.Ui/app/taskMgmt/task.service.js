"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const http_1 = require('@angular/http');
const ng2_webstorage_1 = require('ng2-webstorage');
const TaskDto_1 = require('../TaskDto');
const globals_1 = require('../globals');
let TaskService = class TaskService {
    //private stateList: Array<StateDto> = new Array<StateDto>();
    constructor(http, localStorageService) {
        this.http = http;
        this.localStorageService = localStorageService;
        this.getTasksUrl = globals_1.Globals.BASE_API_URL + '/Task/GetTasksByUser';
        this.editTasksUrl = globals_1.Globals.BASE_API_URL + '/Task/EditTask';
        this.createTaskUrl = globals_1.Globals.BASE_API_URL + '/Task/CreateTask';
        this.deleteTaskUrl = globals_1.Globals.BASE_API_URL + '/Task/DeleteTask';
        this.moveTaskUrl = globals_1.Globals.BASE_API_URL + '/Task/MoveTask';
        this.taskList = new Array();
        this.initTasks();
    }
    initTasks() {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        var headers = new http_1.Headers();
        headers.append('Authorization', 'Bearer ' + authentication.token);
        headers.append('Content-Type', 'application/json');
        let params = new http_1.URLSearchParams();
        params.set('userName', authentication.userName);
        let options = new http_1.RequestOptions({
            method: http_1.RequestMethod.Get,
            url: this.getTasksUrl,
            headers: headers,
            search: params
        });
        this.http.request(new http_1.Request(options))
            .subscribe(data => {
            data.json().forEach(element => this.taskList.push(new TaskDto_1.TaskDto(element)));
        }, err => console.log(err));
    }
    getTasks() {
        return this.taskList;
    }
    editTask(taskDto) {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        var headers = new http_1.Headers();
        headers.append('Authorization', 'Bearer ' + authentication.token);
        headers.append('Content-Type', 'application/json');
        let taskJs = JSON.stringify(taskDto);
        this.http.post(this.editTasksUrl, taskJs, { headers: headers })
            .subscribe(data => {
            let updatedTask = data.json();
            let taskIndex = this.taskList.findIndex(t => t.id == updatedTask.id);
            this.taskList[taskIndex] = updatedTask;
        }, err => console.log(err));
    }
    createTask(taskDto) {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        var headers = new http_1.Headers();
        headers.append('Authorization', 'Bearer ' + authentication.token);
        headers.append('Content-Type', 'application/json');
        taskDto.userName = authentication.userName;
        let taskJs = JSON.stringify(taskDto);
        this.http.post(this.createTaskUrl, taskJs, { headers: headers })
            .subscribe(data => {
            let newTask = data.json();
            this.taskList.push(newTask);
        }, err => console.log(err));
    }
    deleteTask(taskId) {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        var headers = new http_1.Headers();
        headers.append('Authorization', 'Bearer ' + authentication.token);
        headers.append('Content-Type', 'application/json');
        let taskIdJs = JSON.stringify('{ "taskId":' + taskId + '}');
        this.http.post(this.deleteTaskUrl, taskId, { headers: headers })
            .subscribe(data => {
            let taskIndex = this.taskList.findIndex(t => t.id == taskId);
            this.taskList.splice(taskIndex, 1);
        }, err => console.log(err));
    }
    moveTask(moveTaskDto) {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        var headers = new http_1.Headers();
        headers.append('Authorization', 'Bearer ' + authentication.token);
        headers.append('Content-Type', 'application/json');
        let moveTaskJs = JSON.stringify(moveTaskDto);
        this.http.post(this.moveTaskUrl, moveTaskJs, { headers: headers })
            .subscribe(data => {
            let movedTask = data.json();
            let taskIndex = this.taskList.findIndex(t => t.id == movedTask.id);
            this.taskList[taskIndex].state = movedTask.state;
        }, err => console.log(err));
    }
};
TaskService = __decorate([
    core_1.Injectable(), 
    __metadata('design:paramtypes', [http_1.Http, ng2_webstorage_1.LocalStorageService])
], TaskService);
exports.TaskService = TaskService;
//# sourceMappingURL=task.service.js.map