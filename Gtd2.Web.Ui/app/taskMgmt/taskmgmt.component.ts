﻿import { Component } from '@angular/core';
import { DialogService } from '../dialogs/dialog.service';
import { TaskService } from './task.service';
import { TaskDto } from '../TaskDto';

@Component({
	moduleId: module.id,
	templateUrl: 'taskmgmt.component.html',
	styleUrls: [ 'taskmgmt.css' ]
})
export class TaskMgmtComponent {
	private tasks: TaskDto[];

	constructor(
		private taskService: TaskService,
		private dialogService: DialogService) {
			this.tasks = taskService.getTasks();
	}

	showEditTask(id: number) {
		let selectedTask = this.tasks.find(t => t.id == id);
		this.dialogService.showTaskEditDialog(selectedTask)
			.then(dialog => {
				dialog.result.then(returnData => {
					this.taskService.editTask(returnData);
				},
				() => { });
			});
	}

	deleteTask(id: number) {
		this.taskService.deleteTask(id);
	}

	showTaskRegDialog() {
		this.dialogService.showTaskCreateDialog()
			.then(dialog => {
				dialog.result.then(returnData => {
					this.taskService.createTask(returnData);
				},
				() => { });
			});
	}
}