﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, Request, RequestMethod, URLSearchParams } from '@angular/http';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { TaskDto } from '../TaskDto';
import { StateDto } from '../StateDto';
import { MoveTaskDto } from '../MoveTaskDto';
import { Globals } from '../globals';
import { Authentication } from '../auth/authentication';

@Injectable()
export class TaskService {
	private readonly getTasksUrl = Globals.BASE_API_URL + '/Task/GetTasksByUser';
	private readonly editTasksUrl = Globals.BASE_API_URL + '/Task/EditTask';
	private readonly createTaskUrl = Globals.BASE_API_URL + '/Task/CreateTask';
	private readonly deleteTaskUrl = Globals.BASE_API_URL + '/Task/DeleteTask';
	private readonly moveTaskUrl = Globals.BASE_API_URL + '/Task/MoveTask';
	private taskList: Array<TaskDto> = new Array<TaskDto>();
	//private stateList: Array<StateDto> = new Array<StateDto>();

	constructor(private http: Http, private localStorageService: LocalStorageService) {
		this.initTasks();
	}

	private initTasks() {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;

		var headers = new Headers();
		headers.append('Authorization', 'Bearer ' + authentication.token);
		headers.append('Content-Type', 'application/json');

		let params = new URLSearchParams();
		params.set('userName', authentication.userName);

		let options = new RequestOptions({
			method: RequestMethod.Get,
			url: this.getTasksUrl,
			headers: headers,
			search: params
		});

		this.http.request(new Request(options))
			.subscribe(
			data => {
				data.json().forEach(element => this.taskList.push(new TaskDto(element)));
			},
			err => console.log(err)
		);
	}

	getTasks(): TaskDto[] {
		return this.taskList;
	}

	editTask(taskDto: TaskDto) {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;

		var headers = new Headers();
		headers.append('Authorization', 'Bearer ' + authentication.token);
		headers.append('Content-Type', 'application/json');

		let taskJs = JSON.stringify(taskDto);

		this.http.post(this.editTasksUrl, taskJs, { headers: headers })
			.subscribe(
				data => {
					let updatedTask = data.json() as TaskDto;
					let taskIndex = this.taskList.findIndex(t => t.id == updatedTask.id);
					this.taskList[taskIndex] = updatedTask;
				},
				err => console.log(err));
	}

	createTask(taskDto: TaskDto) {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;

		var headers = new Headers();
		headers.append('Authorization', 'Bearer ' + authentication.token);
		headers.append('Content-Type', 'application/json');

		taskDto.userName = authentication.userName;
		let taskJs = JSON.stringify(taskDto);

		this.http.post(this.createTaskUrl, taskJs, { headers: headers })
			.subscribe(
				data => {
					let newTask = data.json() as TaskDto;
					this.taskList.push(newTask);
				},
				err => console.log(err));
	}

	deleteTask(taskId: number) {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;

		var headers = new Headers();
		headers.append('Authorization', 'Bearer ' + authentication.token);
		headers.append('Content-Type', 'application/json');
		
		let taskIdJs = JSON.stringify('{ "taskId":' + taskId + '}');

		this.http.post(this.deleteTaskUrl, taskId, { headers: headers })
			.subscribe(
				data => {
					let taskIndex = this.taskList.findIndex(t => t.id == taskId);
					this.taskList.splice(taskIndex, 1);
				},
				err => console.log(err));
	}

	moveTask(moveTaskDto: MoveTaskDto) {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;

		var headers = new Headers();
		headers.append('Authorization', 'Bearer ' + authentication.token);
		headers.append('Content-Type', 'application/json');

		let moveTaskJs = JSON.stringify(moveTaskDto);

		this.http.post(this.moveTaskUrl, moveTaskJs, { headers: headers })
			.subscribe(
			data => {
				let movedTask = data.json() as TaskDto;
				let taskIndex = this.taskList.findIndex(t => t.id == movedTask.id);
				this.taskList[taskIndex].state = movedTask.state;
			},
			err => console.log(err));
	} 
}