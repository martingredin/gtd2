"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const dialog_service_1 = require('../dialogs/dialog.service');
const task_service_1 = require('./task.service');
let TaskMgmtComponent = class TaskMgmtComponent {
    constructor(taskService, dialogService) {
        this.taskService = taskService;
        this.dialogService = dialogService;
        this.tasks = taskService.getTasks();
    }
    showEditTask(id) {
        let selectedTask = this.tasks.find(t => t.id == id);
        this.dialogService.showTaskEditDialog(selectedTask)
            .then(dialog => {
            dialog.result.then(returnData => {
                this.taskService.editTask(returnData);
            }, () => { });
        });
    }
    deleteTask(id) {
        this.taskService.deleteTask(id);
    }
    showTaskRegDialog() {
        this.dialogService.showTaskCreateDialog()
            .then(dialog => {
            dialog.result.then(returnData => {
                this.taskService.createTask(returnData);
            }, () => { });
        });
    }
};
TaskMgmtComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'taskmgmt.component.html',
        styleUrls: ['taskmgmt.css']
    }), 
    __metadata('design:paramtypes', [task_service_1.TaskService, dialog_service_1.DialogService])
], TaskMgmtComponent);
exports.TaskMgmtComponent = TaskMgmtComponent;
//# sourceMappingURL=taskmgmt.component.js.map