"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const platform_browser_1 = require('@angular/platform-browser');
const forms_1 = require('@angular/forms');
const http_1 = require('@angular/http');
const ng2_webstorage_1 = require('ng2-webstorage');
const angular2_modal_1 = require('angular2-modal');
const bootstrap_1 = require('angular2-modal/plugins/bootstrap');
const ng2_datetime_picker_1 = require('ng2-datetime-picker');
const app_routes_1 = require('./app.routes');
const app_component_1 = require('./app.component');
const auth_component_1 = require('./auth/auth.component');
const frontpage_component_1 = require('./front/frontpage.component');
const statemgmt_component_1 = require('./stateMgmt/statemgmt.component');
const taskmgmt_component_1 = require('./taskMgmt/taskmgmt.component');
const test_component_1 = require('./test/test.component');
const testexttemp_component_1 = require('./test/testexttemp.component');
const informationdialog_component_1 = require('./dialogs/informationdialog.component');
const regform_component_1 = require('./dialogs/regform.component');
const errorreportdialog_component_1 = require('./dialogs/errorreportdialog.component');
const taskeditdialog_component_1 = require('./dialogs/taskeditdialog.component');
const taskcreatedialog_component_1 = require('./dialogs/taskcreatedialog.component');
const auth_service_1 = require('./auth/auth.service');
const dialog_service_1 = require('./dialogs/dialog.service');
const task_service_1 = require('./taskMgmt/task.service');
const stateFilter_pipe_1 = require('./stateMgmt/stateFilter.pipe');
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            ng2_webstorage_1.Ng2Webstorage,
            angular2_modal_1.ModalModule.forRoot(),
            bootstrap_1.BootstrapModalModule,
            app_routes_1.routing,
            ng2_datetime_picker_1.Ng2DatetimePickerModule,
        ],
        declarations: [
            app_component_1.AppComponent,
            auth_component_1.AuthComponent,
            frontpage_component_1.FrontPageComponent,
            statemgmt_component_1.StateMgmtComponent,
            taskmgmt_component_1.TaskMgmtComponent,
            test_component_1.TestComponent,
            testexttemp_component_1.TestComponentWithExternalTemplate,
            informationdialog_component_1.InformationDialog,
            regform_component_1.RegistrationForm,
            errorreportdialog_component_1.ErrorReportDalog,
            taskeditdialog_component_1.TaskEditDialog,
            taskcreatedialog_component_1.TaskCreateDialog,
            stateFilter_pipe_1.StateFilterPipe,
        ],
        entryComponents: [
            informationdialog_component_1.InformationDialog,
            regform_component_1.RegistrationForm,
            errorreportdialog_component_1.ErrorReportDalog,
            taskeditdialog_component_1.TaskEditDialog,
            taskcreatedialog_component_1.TaskCreateDialog,
        ],
        providers: [
            auth_service_1.AuthService,
            dialog_service_1.DialogService,
            task_service_1.TaskService
        ],
        bootstrap: [app_component_1.AppComponent]
    }), 
    __metadata('design:paramtypes', [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map