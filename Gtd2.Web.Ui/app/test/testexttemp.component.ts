﻿import { Component } from '@angular/core';

@Component({
	moduleId: module.id,
	selector: 'testext-component',
	templateUrl: 'testexttemp.template.html'
})
export class TestComponentWithExternalTemplate {
	message = 'test with external template';
}