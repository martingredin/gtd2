﻿import { Component } from '@angular/core';

@Component({
	moduleId: module.id,
	selector: "test-component",
	template: `
		<div>
			<h3>{{message}}</h3>
		</div>
		`
})
export class TestComponent {
	message: string = "test";
}