﻿import { Component, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { Overlay } from 'angular2-modal';

@Component({
	selector: "gtd2",
	templateUrl: 'app/app.component.html'
})
export class AppComponent {
	private hello: string = 'Hello';

	constructor(public vcRef: ViewContainerRef, public overlay: Overlay) {
		overlay.defaultViewContainer = vcRef;
	}
}

