﻿/// <reference path="../../node_modules/@types/jasmine/index.d.ts" />

describe('basic jasmine + karma test', () => {
	it("1 + 1 is 2", () => {
		expect(1 + 1).toBe(2);
	});

	it("5 - 5 is 0", () => {
		expect(5-5).toBe(0);
	});
});