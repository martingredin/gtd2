import { TestBed, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TestComponent } from '../test/test.component';

describe('test of angular 2 test facility, testing the test component', () => {
    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;
    let debugElement: DebugElement;
    let htmlElement: HTMLElement;

    beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [ TestComponent ],
			providers: [
				{ provide: ComponentFixtureAutoDetect, useValue: true }
			]
		});

        fixture = TestBed.createComponent(TestComponent);

        component = fixture.componentInstance; // BannerComponent test instance

        debugElement = fixture.debugElement.query(By.css('h3'));
        htmlElement = debugElement.nativeElement;
    });

    it('should display the standard message', () => {
        expect(htmlElement.textContent).toContain(component.message);
	});

	it('should still display the standard message', () => {
		let newMessage = 'hello world';
		let oldMessage = component.message;
		component.message = newMessage;
		expect(htmlElement.textContent).toContain(oldMessage);
	});

	it('should display the updated message', () => {
		let newMessage = 'hello world';
		component.message = newMessage;
		fixture.detectChanges();
		expect(htmlElement.textContent).toContain(newMessage);
	});
});