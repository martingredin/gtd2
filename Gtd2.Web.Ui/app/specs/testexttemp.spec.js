"use strict";
const testing_1 = require('@angular/core/testing');
const platform_browser_1 = require('@angular/platform-browser');
const testexttemp_component_1 = require('../test/testexttemp.component');
describe('test of angular 2 test facility, testing the test with external template component: ', () => {
    let component;
    let fixture;
    let debugElement;
    let htmlElement;
    //asynchronous beforeEach
    beforeEach(testing_1.async(() => {
        testing_1.TestBed
            .configureTestingModule({
            declarations: [testexttemp_component_1.TestComponentWithExternalTemplate],
            providers: [
                { provide: testing_1.ComponentFixtureAutoDetect, useValue: true }
            ]
        })
            .compileComponents();
    }));
    //synchronous beforeEach
    beforeEach(() => {
        fixture = testing_1.TestBed.createComponent(testexttemp_component_1.TestComponentWithExternalTemplate);
        component = fixture.componentInstance; // BannerComponent test instance
        debugElement = fixture.debugElement.query(platform_browser_1.By.css('h3'));
        htmlElement = debugElement.nativeElement;
    });
    it('should display the standard message', () => {
        expect(htmlElement.textContent).toBe(component.message);
    });
    it('should still display the standard message', () => {
        let newMessage = 'hello world';
        let oldMessage = component.message;
        component.message = newMessage;
        expect(htmlElement.textContent).toBe(oldMessage);
    });
    it('should display the updated message', () => {
        let newMessage = 'hello world';
        component.message = newMessage;
        fixture.detectChanges();
        expect(htmlElement.textContent).toBe(newMessage);
    });
});
//# sourceMappingURL=testexttemp.spec.js.map