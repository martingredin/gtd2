﻿import { async, TestBed, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { TestComponentWithExternalTemplate } from '../test/testexttemp.component';

describe('test of angular 2 test facility, testing the test with external template component: ', () => {
	let component: TestComponentWithExternalTemplate;
	let fixture: ComponentFixture<TestComponentWithExternalTemplate>;
	let debugElement: DebugElement;
	let htmlElement: HTMLElement;

	//asynchronous beforeEach
	beforeEach(async(() => {
		TestBed
			.configureTestingModule({
				declarations: [TestComponentWithExternalTemplate],
				providers: [
					{ provide: ComponentFixtureAutoDetect, useValue: true }
				]
			})
			.compileComponents();
	}));

	//synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(TestComponentWithExternalTemplate);

		component = fixture.componentInstance; // BannerComponent test instance

		debugElement = fixture.debugElement.query(By.css('h3'));
		htmlElement = debugElement.nativeElement;
	});

	it('should display the standard message', () => {
		expect(htmlElement.textContent).toBe(component.message);
	});

	it('should still display the standard message', () => {
		let newMessage = 'hello world';
		let oldMessage = component.message;
		component.message = newMessage;
		expect(htmlElement.textContent).toBe(oldMessage);
	});

	it('should display the updated message', () => {
		let newMessage = 'hello world';
		component.message = newMessage;
		fixture.detectChanges();
		expect(htmlElement.textContent).toBe(newMessage);
	});
});