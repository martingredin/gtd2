"use strict";
const testing_1 = require('@angular/core/testing');
const platform_browser_1 = require('@angular/platform-browser');
const test_component_1 = require('../test/test.component');
describe('test of angular 2 test facility, testing the test component', () => {
    let component;
    let fixture;
    let debugElement;
    let htmlElement;
    beforeEach(() => {
        testing_1.TestBed.configureTestingModule({
            declarations: [test_component_1.TestComponent],
            providers: [
                { provide: testing_1.ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = testing_1.TestBed.createComponent(test_component_1.TestComponent);
        component = fixture.componentInstance; // BannerComponent test instance
        debugElement = fixture.debugElement.query(platform_browser_1.By.css('h3'));
        htmlElement = debugElement.nativeElement;
    });
    it('should display the standard message', () => {
        expect(htmlElement.textContent).toContain(component.message);
    });
    it('should still display the standard message', () => {
        let newMessage = 'hello world';
        let oldMessage = component.message;
        component.message = newMessage;
        expect(htmlElement.textContent).toContain(oldMessage);
    });
    it('should display the updated message', () => {
        let newMessage = 'hello world';
        component.message = newMessage;
        fixture.detectChanges();
        expect(htmlElement.textContent).toContain(newMessage);
    });
});
//# sourceMappingURL=test.spec.js.map