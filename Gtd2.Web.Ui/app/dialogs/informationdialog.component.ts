﻿import { Component } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { InformationDialogData } from './informationdialogdata';

@Component({
	moduleId: module.id,
	selector: 'information-dialog',
	templateUrl: 'informationdialog.component.html'
})
export class InformationDialog implements CloseGuard, ModalComponent<InformationDialogData> {
	informationDialogData: InformationDialogData;

	constructor(public dialog: DialogRef<InformationDialogData>) {
		this.informationDialogData = dialog.context;
		dialog.setCloseGuard(this);
	}

	close() {
		this.dialog.close();
	}
}