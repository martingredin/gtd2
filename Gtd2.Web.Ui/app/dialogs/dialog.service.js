"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const angular2_modal_1 = require('angular2-modal');
const bootstrap_1 = require('angular2-modal/plugins/bootstrap');
const informationdialog_component_1 = require('./informationdialog.component');
const informationdialogdata_1 = require('./informationdialogdata');
const errorreportdialog_component_1 = require('./errorreportdialog.component');
const errorreportdata_1 = require('./errorreportdata');
const regform_component_1 = require('./regform.component');
const UserModelDto_1 = require('../UserModelDto');
const taskeditdialog_component_1 = require('./taskeditdialog.component');
const taskcreatedialog_component_1 = require('./taskcreatedialog.component');
const TaskDto_1 = require('../TaskDto');
let DialogService = class DialogService {
    constructor(modal) {
        this.modal = modal;
    }
    showInformatioinDialog(caption, description) {
        var data = new informationdialogdata_1.InformationDialogData();
        data.caption = caption;
        data.description = description;
        return this.modal.open(informationdialog_component_1.InformationDialog, angular2_modal_1.overlayConfigFactory(data, bootstrap_1.BSModalContext));
    }
    showRegDialog() {
        return this.modal.open(regform_component_1.RegistrationForm, angular2_modal_1.overlayConfigFactory(new UserModelDto_1.UserModelDto, bootstrap_1.BSModalContext));
    }
    showErrorReportDialog() {
        return this.modal.open(errorreportdialog_component_1.ErrorReportDalog, angular2_modal_1.overlayConfigFactory(new errorreportdata_1.ErrorReportData, bootstrap_1.BSModalContext));
    }
    showTaskEditDialog(taskDto) {
        return this.modal.open(taskeditdialog_component_1.TaskEditDialog, angular2_modal_1.overlayConfigFactory(taskDto, bootstrap_1.BSModalContext));
    }
    showTaskCreateDialog() {
        return this.modal.open(taskcreatedialog_component_1.TaskCreateDialog, angular2_modal_1.overlayConfigFactory(new TaskDto_1.TaskDto, bootstrap_1.BSModalContext));
    }
};
DialogService = __decorate([
    core_1.Injectable(), 
    __metadata('design:paramtypes', [bootstrap_1.Modal])
], DialogService);
exports.DialogService = DialogService;
//# sourceMappingURL=dialog.service.js.map