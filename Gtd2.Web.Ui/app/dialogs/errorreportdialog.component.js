"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const angular2_modal_1 = require('angular2-modal');
let ErrorReportDalog = class ErrorReportDalog {
    constructor(dialog) {
        this.dialog = dialog;
        dialog.setCloseGuard(this);
        this.errorReportData = dialog.context;
    }
    ok() {
        this.dialog.close(this.errorReportData);
    }
    cancel() {
        this.dialog.dismiss();
    }
    beforeDismiss() {
        return false;
    }
    beforeClose() {
        return false;
    }
};
ErrorReportDalog = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'errorreport-dialog',
        templateUrl: 'errorreportdialog.component.html'
    }), 
    __metadata('design:paramtypes', [angular2_modal_1.DialogRef])
], ErrorReportDalog);
exports.ErrorReportDalog = ErrorReportDalog;
//# sourceMappingURL=errorreportdialog.component.js.map