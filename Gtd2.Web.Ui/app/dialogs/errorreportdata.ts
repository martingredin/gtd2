﻿export class ErrorReportData {
	public name: string;
	public phoneNumber: string; //maska
	public email: string; //kolla format
	public description: string;
}