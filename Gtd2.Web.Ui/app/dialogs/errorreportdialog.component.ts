﻿import { Component } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { ErrorReportData } from './errorreportdata';

@Component({
	moduleId: module.id,
	selector: 'errorreport-dialog',
	templateUrl: 'errorreportdialog.component.html'
})
export class ErrorReportDalog implements CloseGuard, ModalComponent<ErrorReportData> {
	public errorReportData: ErrorReportData;

	constructor(public dialog: DialogRef<ErrorReportData>) {
		dialog.setCloseGuard(this);
		this.errorReportData = dialog.context;
	}

	ok() {
		this.dialog.close(this.errorReportData);
	}

	cancel() {
		this.dialog.dismiss();
	}

	beforeDismiss(): boolean {
		return false;
	}

	beforeClose(): boolean {
		return false;
	}
}