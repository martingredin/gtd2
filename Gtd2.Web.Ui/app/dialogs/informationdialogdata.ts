﻿export class InformationDialogData {
	public caption: string;
	public description: string;
}