"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const angular2_modal_1 = require('angular2-modal');
let TaskCreateDialog = class TaskCreateDialog {
    constructor(dialog) {
        this.dialog = dialog;
        this.taskDto = dialog.context;
        dialog.setCloseGuard(this);
    }
    ok() {
        this.taskDto.startDate = this.taskDto.startDate + 'T00:00:00';
        this.taskDto.endDate = this.taskDto.endDate + 'T00:00:00';
        this.dialog.close(this.taskDto);
    }
    cancel() {
        this.dialog.dismiss();
    }
    beforeDismiss() {
        return false;
    }
    beforeClose() {
        return false;
    }
};
TaskCreateDialog = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'taskcreate-dialog',
        templateUrl: 'taskcreatedialog.component.html'
    }), 
    __metadata('design:paramtypes', [angular2_modal_1.DialogRef])
], TaskCreateDialog);
exports.TaskCreateDialog = TaskCreateDialog;
//# sourceMappingURL=taskcreatedialog.component.js.map