﻿import { Component, ViewChild, ElementRef } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { TaskDto } from '../TaskDto';

@Component({
	moduleId: module.id,
	selector: 'taskedit-dialog',
	templateUrl: 'taskeditdialog.component.html'
})
export class TaskEditDialog implements CloseGuard, ModalComponent<TaskDto> {
	private taskDto: TaskDto;

	constructor(public dialog: DialogRef<TaskDto>) {
		this.taskDto = dialog.context;
		dialog.setCloseGuard(this);
	}

	ok() {
		this.taskDto.startDate = this.taskDto.startDate + 'T00:00:00';
		this.taskDto.endDate = this.taskDto.endDate + 'T00:00:00';
		this.dialog.close(this.taskDto);
	}

	cancel() {
		this.dialog.dismiss();
	}

	beforeDismiss(): boolean {
		return false;
	}

	beforeClose(): boolean {
		return false;
	}
}