﻿import { Component } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { UserModelDto } from '../UserModelDto';

@Component({
	moduleId: module.id,
	selector: 'reg-form',
	templateUrl: 'regform.component.html'
})
export class RegistrationForm implements CloseGuard, ModalComponent<UserModelDto> {
	public registrationData: UserModelDto;

	constructor(public dialog: DialogRef<UserModelDto>) {
		dialog.setCloseGuard(this);
		this.registrationData = dialog.context;
	}

	ok() {
		this.dialog.close(this.registrationData);
	}

	cancel() {
		this.dialog.dismiss();
	}

	beforeDismiss(): boolean {
		return false;
	}

	beforeClose(): boolean {
		return false;
	}
}