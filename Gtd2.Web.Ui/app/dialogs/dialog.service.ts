﻿import { Injectable } from '@angular/core';
import { overlayConfigFactory, DialogRef } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';

import { InformationDialog } from './informationdialog.component';
import { InformationDialogData } from './informationdialogdata';

import { ErrorReportDalog } from './errorreportdialog.component';
import { ErrorReportData } from './errorreportdata';

import { RegistrationForm } from './regform.component';
import { UserModelDto } from '../UserModelDto';

import { TaskEditDialog } from './taskeditdialog.component';
import { TaskCreateDialog } from './taskcreatedialog.component';
import { TaskDto } from '../TaskDto';

@Injectable()
export class DialogService {
	constructor(private modal: Modal) { }

	showInformatioinDialog(caption: string, description: string): Promise<DialogRef<any>>  {
		var data = new InformationDialogData();
		data.caption = caption;
		data.description = description;

		return this.modal.open(
			InformationDialog,
			overlayConfigFactory(data, BSModalContext));
	}

	showRegDialog(): Promise<DialogRef<UserModelDto>> {
		return this.modal.open(
			RegistrationForm,
			overlayConfigFactory(new UserModelDto, BSModalContext));
	}

	showErrorReportDialog(): Promise<DialogRef<ErrorReportData>> {
		return this.modal.open(
			ErrorReportDalog,
			overlayConfigFactory(new ErrorReportData, BSModalContext));
	}

	showTaskEditDialog(taskDto: TaskDto): Promise<DialogRef<TaskDto>> {
		return this.modal.open(
			TaskEditDialog,
			overlayConfigFactory(taskDto, BSModalContext));
	}

	showTaskCreateDialog(): Promise<DialogRef<TaskDto>> {
		return this.modal.open(
			TaskCreateDialog,
			overlayConfigFactory(new TaskDto, BSModalContext));
	}
}