// Created with Typewriter (http://frhagn.github.io/Typewriter/)
"use strict";
class TaskListDto {
    constructor(data = null) {
        // ID
        this.id = 0;
        // NAME
        this.name = null;
        // DESCRIPTION
        this.description = null;
        // STARTDATE
        this.startDate = null;
        // ENDDATE
        this.endDate = null;
        // STATE
        this.state = null;
        if (data !== null) {
            this.id = data.id;
            this.name = data.name;
            this.description = data.description;
            this.startDate = data.startDate;
            this.endDate = data.endDate;
            this.state = data.state;
        }
    }
}
exports.TaskListDto = TaskListDto;
//# sourceMappingURL=TaskListDto.js.map