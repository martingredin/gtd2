"use strict";
const router_1 = require('@angular/router');
const frontpage_component_1 = require('./front/frontpage.component');
const statemgmt_component_1 = require('./stateMgmt/statemgmt.component');
const taskmgmt_component_1 = require('./taskMgmt/taskmgmt.component');
// Route Configuration
exports.routes = [
    { path: '', redirectTo: '/front', pathMatch: 'full' },
    { path: 'front', component: frontpage_component_1.FrontPageComponent },
    { path: 'stateMgmt', component: statemgmt_component_1.StateMgmtComponent },
    { path: 'taskMgmt', component: taskmgmt_component_1.TaskMgmtComponent }
];
exports.routing = router_1.RouterModule.forRoot(exports.routes);
//# sourceMappingURL=app.routes.js.map