// Created with Typewriter (http://frhagn.github.io/Typewriter/)
"use strict";
class TaskDto {
    constructor(data = null) {
        // ID
        this.id = 0;
        // NAME
        this.name = null;
        // DESCRIPTION
        this.description = null;
        // STARTDATE
        this.startDate = null;
        // ENDDATE
        this.endDate = null;
        // STATE
        this.state = null;
        // USERNAME
        this.userName = null;
        if (data !== null) {
            this.id = data.id;
            this.name = data.name;
            this.description = data.description;
            this.startDate = data.startDate;
            this.endDate = data.endDate;
            this.state = data.state;
            this.userName = data.userName;
        }
    }
}
exports.TaskDto = TaskDto;
//# sourceMappingURL=TaskDto.js.map