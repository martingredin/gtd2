

// Created with Typewriter (http://frhagn.github.io/Typewriter/)


export class MoveTaskDto {
    
    // TASKID
    public taskId: number = 0;
    // DIRECTION
    public direction: number = 0;

    constructor(data: any = null) {
        if (data !== null) {
            this.taskId = data.taskId;
            this.direction = data.direction;
        }
    }
}