﻿import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { DialogService } from '../dialogs/dialog.service';
import { UserModelDto } from '../UserModelDto';

@Component({
	moduleId: module.id,
	templateUrl: 'frontpage.component.html'
})
export class FrontPageComponent {
	constructor(
		private authService: AuthService,
		public dialogService: DialogService
	) { }

	loggedIn() {
		return this.authService.isLoggedIn();
	}

	register() {
		this.dialogService.showRegDialog()
			.then(dialog => {
				dialog.result.then(returnData => {
					this.authService.createUser(returnData)
						.subscribe(
						success => {
							this.dialogService.showInformatioinDialog(
								'Welcome to the Gtd community',
								'Thank you for joining our website, we hope youe life will become more efficient and productive with our help. An email with a confirmation link has been sent to the email address you supplied, please click the link to activate your registration before trying to login.')
						},
						() => { });

				}, () => { });
			});
	}

	reportError() {
		this.dialogService.showErrorReportDialog()
			.then(dialog => {
				dialog.result.then(returnData => {
					//anropa errorreportservice (finns inte än)
					//alert(
					//	'name: ' + returnData.name + '\n' +
					//	'phoneNumber: ' + returnData.phoneNumber + '\n' +
					//	'email: ' + returnData.email + '\n' +
					//	'description: ' + returnData.description);

				});
			});
	}
}