"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const auth_service_1 = require('../auth/auth.service');
const dialog_service_1 = require('../dialogs/dialog.service');
let FrontPageComponent = class FrontPageComponent {
    constructor(authService, dialogService) {
        this.authService = authService;
        this.dialogService = dialogService;
    }
    loggedIn() {
        return this.authService.isLoggedIn();
    }
    register() {
        this.dialogService.showRegDialog()
            .then(dialog => {
            dialog.result.then(returnData => {
                this.authService.createUser(returnData)
                    .subscribe(success => {
                    this.dialogService.showInformatioinDialog('Welcome to the Gtd community', 'Thank you for joining our website, we hope youe life will become more efficient and productive with our help. An email with a confirmation link has been sent to the email address you supplied, please click the link to activate your registration before trying to login.');
                }, () => { });
            }, () => { });
        });
    }
    reportError() {
        this.dialogService.showErrorReportDialog()
            .then(dialog => {
            dialog.result.then(returnData => {
                //anropa errorreportservice (finns inte än)
                //alert(
                //	'name: ' + returnData.name + '\n' +
                //	'phoneNumber: ' + returnData.phoneNumber + '\n' +
                //	'email: ' + returnData.email + '\n' +
                //	'description: ' + returnData.description);
            });
        });
    }
};
FrontPageComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'frontpage.component.html'
    }), 
    __metadata('design:paramtypes', [auth_service_1.AuthService, dialog_service_1.DialogService])
], FrontPageComponent);
exports.FrontPageComponent = FrontPageComponent;
//# sourceMappingURL=frontpage.component.js.map