// Created with Typewriter (http://frhagn.github.io/Typewriter/)
"use strict";
class MoveTaskDto {
    constructor(data = null) {
        // TASKID
        this.taskId = 0;
        // DIRECTION
        this.direction = 0;
        if (data !== null) {
            this.taskId = data.taskId;
            this.direction = data.direction;
        }
    }
}
exports.MoveTaskDto = MoveTaskDto;
//# sourceMappingURL=MoveTaskDto.js.map