﻿${
    // Enable extension methods by adding using Typewriter.Extensions.*
    using Typewriter.Extensions.Types;

    // Uncomment the constructor to change template settings.
    Template(Settings settings)
    {
        settings.IncludeProject("Gtd2.Web.WebServices");
    }

    // Custom extension methods can be used in the template by adding a $ prefix e.g. $LoudName
    string LoudName(Property property)
    {
        return property.Name.ToUpperInvariant();
    }

    string TypeMap(Property property)
    {
        var type = property.Type;

        if (type.IsPrimitive)
        {
            return type.IsDate ?
                $"new Date(data.{property.name})" :
                $"data.{property.name}";
        }
        else
        {
            return type.IsEnumerable ?
                $"data.{property.name}.map(i => new {type.Name.TrimEnd('[', ']')}(i))" :
                $"new {type.Name}(data.{property.name})";
        }
    }
}

// Created with Typewriter (http://frhagn.github.io/Typewriter/)

$Classes(*Dto)[
export class $Name {
    $Properties[
    // $LoudName
    public $name: $Type = $Type[$Default];]

    constructor(data: any = null) {
        if (data !== null) {$Properties[
            this.$name = $TypeMap;]
        }
    }
}]