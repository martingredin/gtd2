﻿export const Globals = Object.freeze({
	AUTH_KEY: 'Auth',
	BASE_URL: 'http://localhost:54759',
	BASE_API_URL: 'http://localhost:54759/Api'
});