// Created with Typewriter (http://frhagn.github.io/Typewriter/)
"use strict";
class StateDto {
    constructor(data = null) {
        // ID
        this.id = 0;
        // NAME
        this.name = null;
        if (data !== null) {
            this.id = data.id;
            this.name = data.name;
        }
    }
}
exports.StateDto = StateDto;
//# sourceMappingURL=StateDto.js.map