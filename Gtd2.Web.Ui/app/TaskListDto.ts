

// Created with Typewriter (http://frhagn.github.io/Typewriter/)


export class TaskListDto {
    
    // ID
    public id: number = 0;
    // NAME
    public name: string = null;
    // DESCRIPTION
    public description: string = null;
    // STARTDATE
    public startDate: string = null;
    // ENDDATE
    public endDate: string = null;
    // STATE
    public state: string = null;

    constructor(data: any = null) {
        if (data !== null) {
            this.id = data.id;
            this.name = data.name;
            this.description = data.description;
            this.startDate = data.startDate;
            this.endDate = data.endDate;
            this.state = data.state;
        }
    }
}