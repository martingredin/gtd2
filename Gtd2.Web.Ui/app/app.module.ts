﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Ng2Webstorage } from 'ng2-webstorage';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { Ng2DatetimePickerModule } from 'ng2-datetime-picker';

import { routing } from './app.routes';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { FrontPageComponent } from './front/frontpage.component';
import { StateMgmtComponent } from './stateMgmt/statemgmt.component';
import { TaskMgmtComponent } from './taskMgmt/taskmgmt.component';
import { TestComponent } from './test/test.component';
import { TestComponentWithExternalTemplate } from './test/testexttemp.component';

import { InformationDialog } from './dialogs/informationdialog.component';
import { RegistrationForm } from './dialogs/regform.component';
import { ErrorReportDalog } from './dialogs/errorreportdialog.component';
import { TaskEditDialog } from './dialogs/taskeditdialog.component';
import { TaskCreateDialog } from './dialogs/taskcreatedialog.component';

import { AuthService } from './auth/auth.service';
import { DialogService } from './dialogs/dialog.service';
import { TaskService } from './taskMgmt/task.service';

import { StateFilterPipe } from './stateMgmt/stateFilter.pipe';

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		Ng2Webstorage,
		ModalModule.forRoot(),
		BootstrapModalModule,
		routing,
		Ng2DatetimePickerModule,
	],
	declarations: [
		AppComponent,
		AuthComponent,
		FrontPageComponent,
		StateMgmtComponent,
		TaskMgmtComponent,
		TestComponent,
		TestComponentWithExternalTemplate,
		InformationDialog,
		RegistrationForm,
		ErrorReportDalog,
		TaskEditDialog,
		TaskCreateDialog,
		StateFilterPipe,
	],
	entryComponents: [
		InformationDialog,
		RegistrationForm,
		ErrorReportDalog,
		TaskEditDialog,
		TaskCreateDialog,
	],
	providers: [
		AuthService,
		DialogService,
		TaskService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
