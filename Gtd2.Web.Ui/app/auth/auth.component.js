"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const router_1 = require('@angular/router');
const auth_service_1 = require('./auth.service');
const UserModelDto_1 = require('../UserModelDto');
const dialog_service_1 = require('../dialogs/dialog.service');
let AuthComponent = class AuthComponent {
    constructor(authService, router, dialogService) {
        this.authService = authService;
        this.router = router;
        this.dialogService = dialogService;
        this.loginData = new UserModelDto_1.UserModelDto();
    }
    login() {
        this.authService.login(this.loginData.userId, this.loginData.password)
            .subscribe(success => {
            this.router.navigateByUrl('/taskMgmt');
            this.loginData = new UserModelDto_1.UserModelDto();
        }, err => {
            let errJs = JSON.parse(err);
            let body = JSON.parse(errJs._body);
            this.dialogService.showInformatioinDialog(body.error, body.error_description);
            this.loginData = new UserModelDto_1.UserModelDto();
        });
    }
    logout() {
        this.authService.logout();
        this.router.navigateByUrl('/front');
    }
    loggedIn() {
        return this.authService.isLoggedIn();
    }
};
AuthComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'auth',
        templateUrl: 'auth.component.html'
    }), 
    __metadata('design:paramtypes', [auth_service_1.AuthService, router_1.Router, dialog_service_1.DialogService])
], AuthComponent);
exports.AuthComponent = AuthComponent;
//# sourceMappingURL=auth.component.js.map