"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const http_1 = require('@angular/http');
const Observable_1 = require('rxjs/Observable');
require('rxjs/Rx'); // use this line if you want to be lazy, otherwise:
require('rxjs/add/operator/map');
require('rxjs/add/operator/do');
require('rxjs/add/observable/throw');
require('rxjs/add/operator/catch');
const ng2_webstorage_1 = require('ng2-webstorage');
const authentication_1 = require('./authentication');
const globals_1 = require('../globals');
let AuthService = class AuthService {
    constructor(http, localStorageService) {
        this.http = http;
        this.localStorageService = localStorageService;
        this.loginUrl = globals_1.Globals.BASE_URL + '/token';
        this.regUrl = globals_1.Globals.BASE_API_URL + '/User/Register';
    }
    createUser(userModelDto) {
        let headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        let options = new http_1.RequestOptions({ headers: headers });
        var data = JSON.stringify(userModelDto);
        return this.http.post(this.regUrl, data, options)
            .map(res => res.json())
            .catch(this.serverError);
    }
    login(userId, password) {
        var credentials = "grant_type=password&username=" + userId + "&password=" + password;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(this.loginUrl, credentials, { headers: headers })
            .map(res => res.json())
            .do(data => {
            var authentication = new authentication_1.Authentication();
            authentication.userName = userId;
            authentication.token = data.access_token;
            authentication.expirationDate = new Date();
            authentication.expirationDate.setSeconds(authentication.expirationDate.getSeconds() + data.expires_in);
            this.localStorageService.store(globals_1.Globals.AUTH_KEY, authentication);
        })
            .catch(this.serverError);
    }
    logout() {
        this.localStorageService.clear(globals_1.Globals.AUTH_KEY);
    }
    isLoggedIn() {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        if (authentication == null)
            return false;
        if (new Date() > authentication.expirationDate)
            return false;
        return true;
    }
    serverError(err) {
        if (err instanceof http_1.Response) {
            var jsString = JSON.stringify(err);
            console.log('There was an error: ' + jsString);
            return Observable_1.Observable.throw(jsString || 'backend server error');
        }
        console.error('There was an error: ' + err);
        return Observable_1.Observable.throw(err || 'backend server error');
    }
    getUserName() {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        return authentication.userName;
    }
    getToken() {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        return authentication.token;
    }
};
AuthService = __decorate([
    core_1.Injectable(), 
    __metadata('design:paramtypes', [http_1.Http, ng2_webstorage_1.LocalStorageService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map