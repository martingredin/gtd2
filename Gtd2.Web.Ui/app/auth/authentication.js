"use strict";
class Authentication {
    constructor() {
        this.userName = "";
        this.token = "";
    }
    timedOut() {
        var now = new Date();
        if (now > this.expirationDate)
            return true;
        return false;
    }
}
exports.Authentication = Authentication;
//# sourceMappingURL=authentication.js.map