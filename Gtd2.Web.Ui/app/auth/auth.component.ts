﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { UserModelDto } from '../UserModelDto';
import { DialogService } from '../dialogs/dialog.service';

@Component({
	moduleId: module.id,
	selector: 'auth',
	templateUrl: 'auth.component.html'
})
export class AuthComponent {
	public loginData: UserModelDto;

	constructor(
		private authService: AuthService,
		private router: Router,
		public dialogService: DialogService
	) {
		this.loginData = new UserModelDto();
	}

	login() {
		this.authService.login(this.loginData.userId, this.loginData.password)
			.subscribe(
				success => {
					this.router.navigateByUrl('/taskMgmt');
					this.loginData = new UserModelDto();
				},
				err => {
					let errJs = JSON.parse(err);
					let body = JSON.parse(errJs._body);
					this.dialogService.showInformatioinDialog(body.error, body.error_description);
					this.loginData = new UserModelDto();
				}
			);
	}
	 
	logout() {
		this.authService.logout();
		this.router.navigateByUrl('/front');
	}

	loggedIn() {
		return this.authService.isLoggedIn();
	}
}