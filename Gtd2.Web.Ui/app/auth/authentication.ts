﻿export class Authentication {
	public userName: string;
	public token: string;
	public expirationDate: Date;

	constructor() {
		this.userName = "";
		this.token = "";
	}

	timedOut() {
		var now = new Date();

		if (now > this.expirationDate)
			return true;

		return false;
	}
}