﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, Request } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';  // use this line if you want to be lazy, otherwise:
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { Authentication } from './authentication';
import { Globals } from '../globals';
import { UserModelDto } from '../UserModelDto';

@Injectable()
export class AuthService {
	private readonly loginUrl = Globals.BASE_URL + '/token';
	private readonly regUrl = Globals.BASE_API_URL + '/User/Register';

	constructor(private http: Http, private localStorageService: LocalStorageService) {}

	createUser(userModelDto: UserModelDto) {
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		var data = JSON.stringify(userModelDto);

		return this.http.post(this.regUrl, data, options)
			.map(res => res.json())
			.catch(this.serverError);
	}

	login(userId: string, password: string) {
		var credentials = "grant_type=password&username=" + userId + "&password=" + password;
		var headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		return this.http.post(this.loginUrl, credentials, { headers: headers })
			.map(res => res.json())
			.do(data => {
				var authentication = new Authentication();
				authentication.userName = userId;
				authentication.token = data.access_token;
				authentication.expirationDate = new Date();
				authentication.expirationDate.setSeconds(authentication.expirationDate.getSeconds() + data.expires_in);

				this.localStorageService.store(Globals.AUTH_KEY, authentication);
			})
			.catch(this.serverError);
	}
	
	logout() {
		this.localStorageService.clear(Globals.AUTH_KEY);
	}

	isLoggedIn() {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;

		if (authentication == null)
			return false;

		if (new Date() > authentication.expirationDate)
			return false;

		return true;
	}

	serverError(err: any) {
		if (err instanceof Response) {
			var jsString = JSON.stringify(err);
			console.log('There was an error: ' + jsString);
			return Observable.throw(jsString || 'backend server error');
		}

		console.error('There was an error: ' + err);
		return Observable.throw(err || 'backend server error');
	}

	getUserName(): string {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;
		return authentication.userName;
	}

	getToken(): string {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;
		return authentication.token;
	}
}