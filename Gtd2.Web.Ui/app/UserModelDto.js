// Created with Typewriter (http://frhagn.github.io/Typewriter/)
"use strict";
class UserModelDto {
    constructor(data = null) {
        // USERID
        this.userId = null;
        // PASSWORD
        this.password = null;
        // CONFIRMPASSWORD
        this.confirmPassword = null;
        if (data !== null) {
            this.userId = data.userId;
            this.password = data.password;
            this.confirmPassword = data.confirmPassword;
        }
    }
}
exports.UserModelDto = UserModelDto;
//# sourceMappingURL=UserModelDto.js.map