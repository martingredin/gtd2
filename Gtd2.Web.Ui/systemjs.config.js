﻿(function (global) {
	System.config({
		typescriptOptions: {
			emitDecoratorMetadata: true,
			experimentalDecorators: true,
			'typeRoots': ['node_modules/@types/'],
			'types': ['jquery']
		},
		paths: {
			'npm:': 'node_modules/'
		},
		map: {
			app: 'app',

			// angular bundles
			'@angular/core': 'npm:@angular/core/bundles/core.umd.js',
			'@angular/common': 'npm:@angular/common/bundles/common.umd.js',
			'@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
			'@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
			'@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
			'@angular/http': 'npm:@angular/http/bundles/http.umd.js',
			'@angular/router': 'npm:@angular/router/bundles/router.umd.js',
			'@angular/router/upgrade': 'npm:@angular/router/bundles/router-upgrade.umd.js',
			'@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
			'@angular/upgrade': 'npm:@angular/upgrade/bundles/upgrade.umd.js',
			'@angular/upgrade/static': 'npm:@angular/upgrade/bundles/upgrade-static.umd.js',

			'rxjs': 'npm:rxjs',
			'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
			'ng2-webstorage': 'npm:ng2-webstorage',
			'angular2-modal': 'npm:angular2-modal',
			'angular2-modal/plugins/bootstrap': 'npm:angular2-modal/bundles',
			'ng2-datetime-picker': 'npm:ng2-datetime-picker/dist'
		},
		packages: {
			app: {
				main: './app/app.main.js',
				defaultExtension: 'js'
			},
			rxjs: {
				defaultExtension: 'js'
			},
			'ng2-webstorage': { main: 'bundles/core.umd.js', defaultExtension: 'js' },
			'angular2-modal': { main: 'bundles/angular2-modal.umd.js', defaultExtension: 'js' },
			'angular2-modal/plugins/bootstrap': { main: 'angular2-modal.bootstrap.umd.js', defaultExtension: 'js' },
			'ng2-datetime-picker': { main: 'ng2-datetime-picker.umd.js', defaultExtension: 'js' }
		}
	});
})(this);