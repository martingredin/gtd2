﻿using Gtd2.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Infrastructure.Data.Mapping
{
	public class Gtd2ContextInitializer : DropCreateDatabaseIfModelChanges<Gtd2Context>
	{
		private KanbanState newState;
		private KanbanState analyzedState;
		private KanbanState inProgressState;
		private KanbanState finishedState;

		private List<User> users = new List<User>();

		private GtdTask task;

		public Gtd2ContextInitializer()
		{
			newState = new KanbanState { Name = "New" };
			analyzedState = new KanbanState { Name = "Analyzed" };
			inProgressState = new KanbanState { Name = "In progress" };
			finishedState = new KanbanState { Name = "Finished" };

			RNGCryptoServiceProvider rngCryptoSP = new RNGCryptoServiceProvider();
			byte[] saltArr = new byte[10];

			rngCryptoSP.GetBytes(saltArr);
			var salt1 = Convert.ToBase64String(saltArr);
			rngCryptoSP.GetBytes(saltArr);
			var salt2 = Convert.ToBase64String(saltArr);

			users.AddRange(new[] {
				User.Create("Martin", "abc123", salt1),
				User.Create("Kalle", "def456", salt2)
			});

			
		}

		protected override void Seed(Gtd2Context context)
		{
			context.KanbaanStates.Add(newState);
			context.KanbaanStates.Add(analyzedState);
			context.KanbaanStates.Add(inProgressState);
			context.KanbaanStates.Add(finishedState);
			context.SaveChanges();

			newState.NextStateId = analyzedState.Id;
			analyzedState.PreviousStateId = newState.Id;
			analyzedState.NextStateId = inProgressState.Id;
			inProgressState.PreviousStateId = analyzedState.Id;
			inProgressState.NextStateId = finishedState.Id;
			finishedState.PreviousStateId = inProgressState.Id;

			context.Users.AddRange(users);
			/*
			task = new GtdTask()
			{
				Description = "test",
				Name = "test",
				StartDate = DateTime.Now,
				EndDate = DateTime.Now.AddDays(10),
				KanbanStateId = newState.Id,
				User = users[0],
				UserId = users[0].Id
			};
			*/
			context.SaveChanges();
			base.Seed(context);
		}
	}
}
