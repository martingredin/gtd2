﻿using Gtd2.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Infrastructure.Data.Mapping
{
	public class Gtd2Context : DbContext
	{
		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<GtdTask> GtdTasks { get; set; }
		public virtual DbSet<KanbanState> KanbaanStates { get; set; }

		private const string dbName = "Gtd2Db";

		public Gtd2Context()
			: base(dbName)
		{
			Database.SetInitializer<Gtd2Context>(new Gtd2ContextInitializer());
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<GtdTask>()
				.HasMany(m => m.SubTasks)
				.WithRequired(m => m.GtdTask);

			modelBuilder.Entity<User>()
				.HasMany(r => r.Tasks)
				.WithRequired(r => r.User);
		}
	}
}
