﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Infrastructure.Data.Repositories
{
	public class TaskRepository : ITaskRepository
	{
		private Gtd2Context dataContext;

		public TaskRepository(Gtd2Context context)
		{
			dataContext = context;
		}

		public void SaveTask(GtdTask task)
		{
			dataContext.GtdTasks.Add(task);
			dataContext.SaveChanges();
		}

		public GtdTask GetById(int id)
		{
			return (from t in dataContext.GtdTasks
					where t.Id == id
					select t).FirstOrDefault();
		}

		public void Add(GtdTask entity)
		{
			throw new NotImplementedException();
		}

		public void Remove(GtdTask entity)
		{
			dataContext.GtdTasks.Remove(entity);
		}

		public IEnumerable<GtdTask> List()
		{
			return (from t in dataContext.GtdTasks
					select t).ToList();
		}

		public void SaveChanges()
		{
			throw new NotImplementedException();
		}
	}
}