﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Infrastructure.Data.Repositories
{
	public class UserRepository : IUserRepository
	{
		private Gtd2Context gtdContext;

		public UserRepository(Gtd2Context context)
		{
			gtdContext = context;
		}


		public User GetUser(string userName)
		{
			return (from u in gtdContext.Users
					where u.UserName == userName
					select u).FirstOrDefault();
		}

		public void AddUserTasks(User user, GtdTask task)
		{
			user.Tasks.Add(task);
		}

		public User GetById(int id)
		{
			return (from u in gtdContext.Users
					where u.Id == id
					select u).FirstOrDefault();
		}

		public void Add(User entity)
		{
			throw new NotImplementedException();
		}

		public void Remove(User entity)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<User> List()
		{
			return (from u in gtdContext.Users
					select u).ToList();
		}

		public void SaveChanges()
		{
			throw new NotImplementedException();
		}
	}
}
