﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Infrastructure.Data.Repositories
{
	public class StateRepository : IStateRepository
	{
		private Gtd2Context dbContext;

		public StateRepository(Gtd2Context context)
		{
			dbContext = context;
		}

		public KanbanState GetStateByName(string name)
		{
			return (from s in dbContext.KanbaanStates
					where s.Name == name
					select s).FirstOrDefault();
		}

		public KanbanState GetStateById(int id)
		{
			return (from s in dbContext.KanbaanStates
					where s.Id == id
					select s).FirstOrDefault();
		}

		public KanbanState GetById(int id)
		{
			return (from s in dbContext.KanbaanStates
					where s.Id == id
					select s).FirstOrDefault();
		}

		public IEnumerable<KanbanState> List()
		{
			return (from s in dbContext.KanbaanStates
					select s).ToList();
		}

		public void Add(KanbanState entity)
		{
			throw new NotImplementedException();
		}

		public void Remove(KanbanState entity)
		{
			throw new NotImplementedException();
		}

		public void SaveChanges()
		{
			throw new NotImplementedException();
		}
	}
}