﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Exceptions;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Infrastructure.Data.Repositories;
using Gtd2.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Gtd2.Services
{
	public class TaskService : ITaskService
	{
		private Gtd2Context dbContext;
		private IUserRepository userRepository;
		private IStateRepository stateRepository;
		private ITaskRepository taskRepository;

		public TaskService(Gtd2Context dbContext)
			: this(dbContext, new UserRepository(dbContext), new StateRepository(dbContext), null)
		{
			this.dbContext = dbContext;
		}

		public TaskService(Gtd2Context dbContext , IUserRepository userRepository, IStateRepository stateRepository, ITaskRepository taskRepository)
		{
			this.dbContext = dbContext;
			this.userRepository = userRepository;
			this.stateRepository = stateRepository;
			this.taskRepository = taskRepository;
		}

		public GtdTask AddTask(string userName, string name, string description, DateTime? startDate, DateTime? endDate)
		{
			var user = userRepository.GetUser(userName);
			var task = GtdTask.Create(name, description, startDate, endDate);
			task.KanbanStateId = stateRepository.GetStateByName("New").Id;
			task.User = user;
			user.Tasks.Add(task);

			dbContext.SaveChanges();

			return task;
		}

		public GtdTask UpdateTask(int taskId, string newName, string newDescription, DateTime? startDate, DateTime? endDate)
		{
			var task = taskRepository.GetById(taskId);
			task.Name = newName;
			task.Description = newDescription;
			task.StartDate = startDate;
			task.EndDate = endDate;
			dbContext.SaveChanges();

			return task;
		}

		public string MoveTaskToState(int taskId, Direction direction)
		{
			var task = taskRepository.GetById(taskId);
			var state = stateRepository.GetById((int)task.KanbanStateId);

			switch (direction)
			{
				case Direction.Backwards:
					if (state.PreviousStateId == null)
						throw new CantMoveBackwardsException();

					task.KanbanStateId = state.PreviousStateId;
					break;

				case Direction.Forwards:
					if (state.NextStateId == null)
						throw new CantMoveForwardsException();

					task.KanbanStateId = state.NextStateId;
					break;
			}
			
			dbContext.SaveChanges();

			return stateRepository.GetById((int)task.KanbanStateId).Name;
		}

		public void ChangeTaskToState(int taskId, int stateId)
		{
			var task = taskRepository.GetById(taskId);
			var state = stateRepository.GetById(stateId);
			task.KanbanStateId = state.Id;
			dbContext.SaveChanges();
		}

		public GtdTask GetTask(int taskId)
		{
			return taskRepository.GetById(taskId);
		}

		public IEnumerable<GtdTask> GetAllTasks()
		{
			return taskRepository.List();
		}

		public IEnumerable<GtdTask> GetTaskForUser(string userName)
		{
			var user = userRepository.GetUser(userName);
			return user.Tasks;
		}

		public void DeleteTask(int taskId)
		{
			var task = taskRepository.GetById(taskId);
			dbContext.GtdTasks.Remove(task);
			dbContext.SaveChanges();
		}
	}
}
