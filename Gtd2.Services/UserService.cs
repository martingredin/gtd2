﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Exceptions;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Infrastructure.Data.Repositories;
using Gtd2.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Services
{
	public class UserService : IUserService
	{
		Gtd2Context dbContext;
		private IUserRepository userRepository;

		public UserService(Gtd2Context context)
			: this(context, new UserRepository(context))
		{ }

		public UserService(Gtd2Context context, IUserRepository userRepository)
		{
			this.dbContext = context;
			this.userRepository = userRepository;
		}

		public bool AuthenticateUser(string userName, string password)
		{
			var user = userRepository.GetUser(userName);

			if (user == null)
				return false;

			return user.Authenticate(userName, password);
		}

		public void RegisterUser(string userName, string password, string confirmPassword)
		{
			if (password != confirmPassword)
				throw new RepeatPasswordException();

			var existingUser = userRepository.GetUser(userName);
			if (existingUser != null)
				throw new UserAlreadyExistsException(userName);

			RNGCryptoServiceProvider rngCryptoSP = new RNGCryptoServiceProvider();
			byte[] saltArr = new byte[10];

			rngCryptoSP.GetBytes(saltArr);
			var salt = Convert.ToBase64String(saltArr);

			var user = User.Create(userName, password, salt);

			dbContext.Users.Add(user);
			dbContext.SaveChanges();
		}

		public User GetUser(string userName)
		{
			return userRepository.GetUser(userName);
		}
	}
}
