﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gtd2.Services.Interfaces;
using Gtd2.Domain.Entities;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Domain.Interfaces;
using Gtd2.Domain.Exceptions;

namespace Gtd2.Services
{
	public class StateService : IStateService
	{
		IStateRepository stateRepository;

		public StateService(IStateRepository stateRepository)
		{
			this.stateRepository = stateRepository;
		}

		public KanbanState GetState(int stateId)
		{
			var state = stateRepository.GetStateById(stateId);

			if (state == null)
				throw new NoSuchStateException(stateId);

			return state;
		}

		public IEnumerable<KanbanState> GetAllStates()
		{
			return stateRepository.List();
		}
	}
}
