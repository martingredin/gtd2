﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gtd2.Web.WebServices.Models
{
	public class TaskListDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public string State { get; set; }
	}
}