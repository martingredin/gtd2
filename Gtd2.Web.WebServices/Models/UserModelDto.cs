﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gtd2.Web.WebServices.Models
{
	public class UserModelDto
	{
		public string UserId { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }
	}
}