﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gtd2.Web.WebServices.Models
{
	public class StateDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}