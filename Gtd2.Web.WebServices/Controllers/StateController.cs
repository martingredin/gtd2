﻿using Gtd2.Domain.Entities;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Infrastructure.Data.Repositories;
using Gtd2.Services;
using Gtd2.Services.Interfaces;
using Gtd2.Web.WebServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gtd2.Web.WebServices.Controllers
{
    public class StateController : ApiController
    {
		private IStateService stateService;

		public StateController()
			: this(new StateService(new StateRepository(new Gtd2Context())))
		{ }

		public StateController(IStateService stateService)
		{
			this.stateService = stateService;
		}

		[HttpGet]
		[Authorize]
		[ActionName("GetAllStates")]
		public StateDto[] GetAllStates()
		{
			var allStates = stateService.GetAllStates().ToList();

			List<StateDto> dtos = new List<StateDto>();
			foreach (var state in allStates)
			{
				dtos.Add(new StateDto { Id = state.Id, Name = state.Name });
			}


			return dtos.ToArray();
		}
    }
}
