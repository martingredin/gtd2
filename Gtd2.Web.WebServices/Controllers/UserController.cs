﻿using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Services;
using Gtd2.Services.Interfaces;
using Gtd2.Web.WebServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Gtd2.Web.WebServices.Controllers
{
	public class UserController : ApiController
	{
		private IUserService userService;

		public UserController()
		{
			userService = new UserService(new Gtd2Context());
		}

		public UserController(IUserService userService)
		{
			this.userService = userService;
		}

		[HttpPost]
		[AllowAnonymous]
		[ActionName("Register")]
		public IHttpActionResult Register([FromBody]UserModelDto userModel)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			try
			{
				userService.RegisterUser(userModel.UserId, userModel.Password, userModel.ConfirmPassword);
			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}

			return Ok();
		}
	}
}