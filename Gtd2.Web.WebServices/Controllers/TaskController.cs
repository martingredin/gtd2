﻿using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Infrastructure.Data.Repositories;
using Gtd2.Services;
using Gtd2.Services.Interfaces;
using Gtd2.Web.WebServices.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Gtd2.Web.WebServices.Controllers
{
	public class TaskController : ApiController
	{
		private ITaskService taskService;
		private IUserService userService;
		private IStateService stateService;

		public TaskController()
		{
			var dbContext = new Gtd2Context();
			taskService = new TaskService(dbContext, new UserRepository(dbContext), new StateRepository(dbContext), new TaskRepository(dbContext));
			userService = new UserService(dbContext, new UserRepository(dbContext));
			stateService = new StateService(new StateRepository(dbContext));
		}

		public TaskController(ITaskService taskService, IUserService userService, IStateService stateService)
		{
			this.taskService = taskService;
			this.userService = userService;
			this.stateService = stateService;
		}

		[HttpGet]
		[Authorize]
		[ActionName("GetTasksByUser")]
		public TaskDto[] GetTasksByUser(string userName)
		{
			var user = userService.GetUser(userName);

			var dtos = new List<TaskDto>();

			foreach (var task in user.Tasks)
			{
				var dto = new TaskDto
				{
					Id = task.Id,
					Name = task.Name,
					Description = task.Description,
					StartDate = ((DateTime)task.StartDate).ToString("yyyy-MM-dd"),
					EndDate = ((DateTime)task.EndDate).ToString("yyyy-MM-dd"),
					State = stateService.GetState((int)task.KanbanStateId).Name,
					UserName = userName
				};

				dtos.Add(dto);
			}

			return dtos.ToArray();
		}

		[HttpPost]
		[Authorize]
		[ActionName("CreateTask")]
		public TaskDto CreateTask(TaskDto taskDto)
		{
			DateTime? startDate = !string.IsNullOrEmpty(taskDto.StartDate) ? DateTime.Parse(taskDto.StartDate) as DateTime? : null;
			DateTime? endDate = !string.IsNullOrEmpty(taskDto.EndDate) ? DateTime.Parse(taskDto.EndDate) as DateTime? : null;

			var newTask = taskService.AddTask(taskDto.UserName, taskDto.Name, taskDto.Description, startDate, endDate);

			var retdto = new TaskDto
			{
				Id = newTask.Id,
				Name = newTask.Name,
				Description = newTask.Description,
				StartDate = newTask.StartDate != null ? ((DateTime)newTask.StartDate).ToString("yyyy-MM-dd") : "",
				EndDate = newTask.EndDate != null ? ((DateTime)newTask.EndDate).ToString("yyyy-MM-dd") : "",
				State = stateService.GetState((int)newTask.KanbanStateId).Name,
				UserName = taskDto.UserName
			};

			return retdto;
		}

		[HttpPost]
		[Authorize]
		[ActionName("EditTask")]
		public TaskDto EditTask(TaskDto taskDto)
		{
			DateTime? startDate = !string.IsNullOrEmpty(taskDto.StartDate) ? DateTime.Parse(taskDto.StartDate) as DateTime? : null;
			DateTime? endDate = !string.IsNullOrEmpty(taskDto.EndDate) ? DateTime.Parse(taskDto.EndDate) as DateTime? : null;

			var updatedTask = taskService.UpdateTask(taskDto.Id, taskDto.Name, taskDto.Description, startDate, endDate);

			var retdto = new TaskDto
			{
				Id = updatedTask.Id,
				Name = updatedTask.Name,
				Description = updatedTask.Description,
				StartDate = updatedTask.StartDate != null ? ((DateTime)updatedTask.StartDate).ToString("yyyy-MM-dd") : "",
				EndDate = updatedTask.EndDate != null ? ((DateTime)updatedTask.EndDate).ToString("yyyy-MM-dd") : "",
				State = stateService.GetState((int)updatedTask.KanbanStateId).Name,
				UserName = taskDto.UserName
			};

			return retdto;
		}

		[HttpPost]
		[Authorize]
		[ActionName("DeleteTask")]
		public void DeleteTask([FromBody]int taskId)
		{
			taskService.DeleteTask(taskId);
		}

		[HttpPost]
		[Authorize]
		[ActionName("MoveTask")]
		public TaskDto MoveTask(MoveTaskDto dto)
		{
			Direction d = (Direction)dto.Direction;
			var newStateName = taskService.MoveTaskToState(dto.TaskId, d);
			return new TaskDto { Id = dto.TaskId, State = newStateName };
		}
	}
}