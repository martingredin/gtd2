var Gtd2;
(function (Gtd2) {
    var Web;
    (function (Web) {
        var WebServices;
        (function (WebServices) {
            // $Classes/Enums/Interfaces(filter)[template][separator]
            // filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
            // template: The template to repeat for each matched item
            // separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
            // More info: http://frhagn.github.io/Typewriter/
            var StateDto = (function () {
                function StateDto(data) {
                    if (data === void 0) { data = null; }
                    // ID
                    this.id = 0;
                    // NAME
                    this.name = null;
                    if (data !== null) {
                        this.id = data.id;
                        this.name = data.name;
                    }
                }
                return StateDto;
            })();
            WebServices.StateDto = StateDto;
        })(WebServices = Web.WebServices || (Web.WebServices = {}));
    })(Web = Gtd2.Web || (Gtd2.Web = {}));
})(Gtd2 || (Gtd2 = {}));
//# sourceMappingURL=StateDto.js.map