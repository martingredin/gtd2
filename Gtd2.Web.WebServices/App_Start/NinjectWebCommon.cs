[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Gtd2.Web.WebServices.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Gtd2.Web.WebServices.App_Start.NinjectWebCommon), "Stop")]

namespace Gtd2.Web.WebServices.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
	using Ninject.Web.WebApi;
	using System.Web.Http;
	using Gtd2.Infrastructure.Data.Mapping;
	using Gtd2.Domain.Interfaces;
	using Gtd2.Infrastructure.Data.Repositories;
	using Gtd2.Services.Interfaces;
	using Gtd2.Services;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
				GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
			kernel.Bind<Gtd2Context>().To<Gtd2Context>();
			kernel.Bind<IStateRepository>().To<StateRepository>();
			kernel.Bind<ITaskRepository>().To<TaskRepository>();
			kernel.Bind<IUserRepository>().To<UserRepository>();
			kernel.Bind<IStateService>().To<StateService>();
			kernel.Bind<ITaskService>().To<TaskService>();
			kernel.Bind<IUserService>().To<UserService>();
        }        
    }
}
