var Gtd2;
(function (Gtd2) {
    var Web;
    (function (Web) {
        var WebServices;
        (function (WebServices) {
            // $Classes/Enums/Interfaces(filter)[template][separator]
            // filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
            // template: The template to repeat for each matched item
            // separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
            // More info: http://frhagn.github.io/Typewriter/
            var UserModelDto = (function () {
                function UserModelDto(data) {
                    if (data === void 0) { data = null; }
                    // USERID
                    this.userId = null;
                    // PASSWORD
                    this.password = null;
                    // CONFIRMPASSWORD
                    this.confirmPassword = null;
                    if (data !== null) {
                        this.userId = data.userId;
                        this.password = data.password;
                        this.confirmPassword = data.confirmPassword;
                    }
                }
                return UserModelDto;
            })();
            WebServices.UserModelDto = UserModelDto;
        })(WebServices = Web.WebServices || (Web.WebServices = {}));
    })(Web = Gtd2.Web || (Gtd2.Web = {}));
})(Gtd2 || (Gtd2 = {}));
//# sourceMappingURL=UserModelDto.js.map