var Gtd2;
(function (Gtd2) {
    var Web;
    (function (Web) {
        var WebServices;
        (function (WebServices) {
            // $Classes/Enums/Interfaces(filter)[template][separator]
            // filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
            // template: The template to repeat for each matched item
            // separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]
            // More info: http://frhagn.github.io/Typewriter/
            var TaskDto = (function () {
                function TaskDto(data) {
                    if (data === void 0) { data = null; }
                    // ID
                    this.id = 0;
                    // TASKNAME
                    this.taskName = null;
                    // DESCRIPTION
                    this.description = null;
                    // STARTDATE
                    this.startDate = null;
                    // ENDDATE
                    this.endDate = null;
                    // USERNAME
                    this.userName = null;
                    if (data !== null) {
                        this.id = data.id;
                        this.taskName = data.taskName;
                        this.description = data.description;
                        this.startDate = data.startDate;
                        this.endDate = data.endDate;
                        this.userName = data.userName;
                    }
                }
                return TaskDto;
            })();
            WebServices.TaskDto = TaskDto;
        })(WebServices = Web.WebServices || (Web.WebServices = {}));
    })(Web = Gtd2.Web || (Gtd2.Web = {}));
})(Gtd2 || (Gtd2 = {}));
//# sourceMappingURL=TaskDto.js.map