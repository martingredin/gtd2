﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Domain.Entities
{
	public class KanbanState : Entity<int>
	{
		public string Name { get; set; }
		public int? PreviousStateId { get; set; }
		public int? NextStateId { get; set; }
	}
}