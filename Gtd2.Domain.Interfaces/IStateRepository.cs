﻿using Gtd2.Domain.Entities;
using Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Domain.Interfaces
{
	public interface IStateRepository : IRepository<KanbanState>
	{
		KanbanState GetStateByName(string name);
		KanbanState GetStateById(int id);
	}
}
