﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Infrastructure.Data.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;

namespace Gtd2.Infrastructure.Test.RepositoryTests
{
	[TestFixture]
	public class TaskRepositoryTests
	{
		GtdTask task;
		User user;
		private string userName = "Martin", name = "newtask", description = "a new task";
		private DateTime? startDate = new DateTime(2001, 01, 01, 10, 01, 01), endDate = new DateTime(2002, 01, 01, 10, 01, 01);

		ITaskRepository target;
		private string connectionString = @"Server=(local)\SQLEXPRESS;Database=Gtd2Db;Trusted_Connection=True;";
		SqlConnection con = null;

		[SetUp]
		public void SetUp()
		{
			target = new TaskRepository(new Gtd2Context());

			task = GtdTask.Create(name, description, startDate, endDate);

			con = new SqlConnection(connectionString);

			user = con.Query<User>("select * from Users where UserName = @userName", new { @userName = userName }).FirstOrDefault();
		}

		[TearDown]
		public void TearDown()
		{
			con.Execute("delete GtdTasks where Id = @id", new { @id = task.Id });

			con.Close();
			con = null;

			target = null;
			task = null;
		}

		[Test]
		public void G_ValidTask_W_SavingTask_T_TaskIsAddedToDatabase()
		{
			task.User = user;
			task.KanbanStateId = 1;
			target.SaveTask(task);
			
			int? noTasks = con.Query<int>("select count(*) from GtdTasks where Id = @id", new { @id = task.Id }).FirstOrDefault();

			Assert.IsNotNull(noTasks);
			Assert.AreEqual(1, noTasks);
		}
	}
}
