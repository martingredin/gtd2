﻿using Gtd2.Domain.Entities;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Infrastructure.Data.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Infrastructure.Test.RepositoryTests
{
	[TestFixture]
	public class StateRepositoryTests
	{
		private StateRepository target;

		[SetUp]
		public void SetUp()
		{
			target = new StateRepository(new Gtd2Context());
		}

		[TearDown]
		public void TearDown()
		{
			target = null;
		}

		[Test]
		public void G_CorrectStateName_W_FetchingFromDatabase_T_WillReturnCorrespondingState()
		{
			string stateName = "Analyzed";

			var result = target.GetStateByName(stateName);

			Assert.AreEqual(result.Name, stateName);
		}

		[Test]
		public void G_IncorrectStateName_W_FetchingFromDatabase_T_WillReturnNull()
		{
			string stateName = "Analyzasdefed";

			var result = target.GetStateByName(stateName);

			Assert.IsNull(result);
		}

		[Test]
		public void G_CorrectStateId_W_FetchingFromDatabase_T_WillReturnCorrespondingState()
		{
			var stateId = 1;

			var result = target.GetStateById(stateId);

			Assert.AreEqual(result.Id, stateId);
		}

		[Test]
		public void G_IncorrectStateId_W_FetchingFromDatabase_T_WillReturnCorrespondingState()
		{
			var stateId = 10;

			var result = target.GetStateById(stateId);

			Assert.IsNull(result);
		}

		[Test]
		public void G_Whatever_W_FetchingFromDatabase_T_GetAllStates()
		{
			var result = target.List();

			CollectionAssert.IsNotEmpty(result);
		}

	}
}
