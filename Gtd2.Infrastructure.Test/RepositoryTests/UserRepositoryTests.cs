﻿using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Infrastructure.Data.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Infrastructure.Test.RepositoryTests
{
	[TestFixture]
	public class UserRepositoryTests
	{
		private IUserRepository target;

		[SetUp]
		public void SetUp()
		{
			target = new UserRepository(new Gtd2Context());
		}

		[TearDown]
		public void TearDown()
		{
			target = null;
		}

		[Test]
		public void G_CorrectUsername_W_FetchingFromDb_T_WillReturnCorrespondingUser()
		{
			var userName = "Martin";

			var result = target.GetUser(userName);

			Assert.AreEqual(result.UserName, userName);
		}

		[Test]
		public void G_IncorrectUsername_W_FetchingFromDb_T_WillReturnNull()
		{
			var userName = "Martincghdrg";

			var result = target.GetUser(userName);

			Assert.IsNull(result);
		}
	}
}
