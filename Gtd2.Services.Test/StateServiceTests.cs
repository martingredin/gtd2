﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Exceptions;
using Gtd2.Domain.Interfaces;
using Gtd2.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Services.Test.StateServiceTests
{
	[TestFixture]
	public class StateServiceTests
	{
		private IStateService target;

		[Test]
		public void G_CorrectId_W_RetrieveingState_T_ReturnsCorrespondingState()
		{
			var stateId = 1;
			var mock = new Mock<IStateRepository>();
			mock.Setup(foo => foo.GetStateById(It.IsIn(stateId))).Returns(new KanbanState { Name = "New" });
			target = new StateService(mock.Object);

			var state = target.GetState(stateId);

			target = null;

			Assert.AreEqual(state.Name, "New");
		}

		[Test]
		public void G_IncorrectId_W_RetrieveingState_T_ThrowsNoSuchStateException()
		{
			var stateId = 10;
			var mock = new Mock<IStateRepository>();
			mock.Setup(foo => foo.GetStateById(It.IsNotIn(1, 5))).Returns<KanbanState>(null);

			target = new StateService(mock.Object);

			Assert.Throws(typeof(NoSuchStateException), () => target.GetState(stateId));
			
			target = null;
		}


	}
}
