﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Test;
using Gtd2.Infrastructure.Data.Repositories;
using Gtd2.Domain.Exceptions;

namespace Gtd2.Services.Test
{
	[TestFixture]
	public class TaskServiceTests
	{
		private User[] fakeUsers = new User[] 
 		{
			new User { Id = 1, UserName = "Martin", Tasks = new List<GtdTask>() }
		};

		private KanbanState[] fakeStates = new KanbanState[4];

		private GtdTask[] fakeTasks = new GtdTask[]
		{
			new GtdTask {Id = 1, Name = "TEST", KanbanStateId = 2 },
			new GtdTask {Id = 2, Name = "TEST", KanbanStateId = 1 },
			new GtdTask {Id = 3, Name = "TEST", KanbanStateId = 4 }
		};

		private Mock<Gtd2Context> mockedContext = new Mock<Gtd2Context>();

		private IUserRepository userRepositoryMock;
		private IStateRepository stateRepositoryMock;
		private ITaskRepository taskRepositoryMock;

		private string userName = "Martin", name = "newtask", description = "a new task";
		private DateTime? startDate = new DateTime(2001, 01, 01, 10, 01, 01), endDate = new DateTime(2002, 01, 01, 10, 01, 01);

		[SetUp]
		public void SetUp()
		{
			KanbanState newState = new KanbanState { Id = 1, Name = "New" };
			KanbanState analyzedState = new KanbanState { Id = 2, Name = "Analyzed" };
			KanbanState inProgressState = new KanbanState { Id = 3, Name = "In progress" };
			KanbanState finishedState = new KanbanState { Id = 4, Name = "Finished" };

			newState.NextStateId = analyzedState.Id;
			analyzedState.PreviousStateId = newState.Id;
			analyzedState.NextStateId = inProgressState.Id;
			inProgressState.PreviousStateId = analyzedState.Id;
			inProgressState.NextStateId = finishedState.Id;
			finishedState.PreviousStateId = inProgressState.Id;

			fakeStates[0] = newState;
			fakeStates[1] = analyzedState;
			fakeStates[2] = inProgressState;
			fakeStates[3] = finishedState;

			fakeUsers[0].Tasks.Add(fakeTasks[0]);
			fakeTasks[0].User = fakeUsers[0];
			fakeTasks[0].UserId = fakeUsers[0].Id;
			fakeUsers[0].Tasks.Add(fakeTasks[2]);
			fakeTasks[2].User = fakeUsers[0];
			fakeTasks[2].UserId = fakeUsers[0].Id;

			mockedContext.Setup(c => c.Users).ReturnsDbSet(fakeUsers);
			mockedContext.Setup(c => c.KanbaanStates).ReturnsDbSet(fakeStates);
			mockedContext.Setup(c => c.GtdTasks).ReturnsDbSet(fakeTasks);

			userRepositoryMock = new UserRepository(mockedContext.Object);
			stateRepositoryMock = new StateRepository(mockedContext.Object);
			taskRepositoryMock = new TaskRepository(mockedContext.Object);
		}
		
		[Test]
		public void G_CorrectInputData_W_AddingTask_T_ReturnsNewTask()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, null);

			var task = target.AddTask(userName, name, description, startDate, endDate);

			Assert.AreEqual(task.User.UserName, userName);
			Assert.AreEqual(task.Name, name);
			Assert.AreEqual(task.Description, description);
			Assert.AreEqual(task.StartDate, startDate);
			Assert.AreEqual(task.EndDate, endDate);
		}

		[Test]
		public void G_ACorrectTask_W_ChangingData_T_dataIsChanged()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);
			var nyttnamn = "nytttest";
			var nybeskr = "blabla";
			DateTime? startDate = null;
			DateTime? endDate = null;

			var result = target.UpdateTask(1, nyttnamn, nybeskr, startDate, endDate);

			Assert.AreEqual(result.Name, nyttnamn);
			Assert.AreEqual(result.Description, nybeskr);
		}

		[Test]
		public void G_AnAnalyzedTask_W_MovingForwards_T_TaskIsInProgress()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);

			var state = target.MoveTaskToState(1, Direction.Forwards);

			Assert.AreEqual(state, fakeStates[2].Name);
		}

		[Test]
		public void G_AnAnalyzedTask_W_MovingBackwards_T_TaskIsInProgress()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);

			var state = target.MoveTaskToState(1, Direction.Backwards);

			Assert.AreEqual(state, fakeStates[0].Name);
		}

		[Test]
		public void G_ANewTask_W_MovingBackwards_T_ThrowsCantMoveBackwardsException()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);

			Assert.Throws(typeof(CantMoveBackwardsException), () =>
				{
					var state = target.MoveTaskToState(2, Direction.Backwards);
				});
		}

		[Test]
		public void G_ANewTask_W_MovingForwards_T_ThrowsCantMoveForwardsException()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);

			Assert.Throws(typeof(CantMoveForwardsException), () =>
			{
				var state = target.MoveTaskToState(3, Direction.Forwards);
			});
		}

		[Test]
		public void G_TaskWithStateNew_W_ChangingStateToFinished_T_TaskWillHaveStateFinished()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);
			var newStateId = 4;

			target.ChangeTaskToState(fakeTasks[1].Id, newStateId);

			Assert.AreEqual(fakeTasks[1].KanbanStateId, newStateId);
		}

		[Test]
		public void G_ACorrectTaskId_W_GettingTask_T_TaskIsReturned()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);
			var taskId = 1;

			var task = target.GetTask(taskId);

			Assert.AreEqual(task.Id, taskId);
		}

		[Test]
		public void G_AnIncorrectTaskId_W_GettingTask_T_NullIsReturned()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);
			var taskId = 6;

			var task = target.GetTask(taskId);

			Assert.IsNull(task);
		}

		[Test]
		public void G_AListOfTasks_W_GettingAllTasks_T_AllTasksAreReturned()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);

			var tasks = target.GetAllTasks();

			CollectionAssert.IsNotEmpty(tasks);
		}

		[Test]
		public void G_AValidUser_W_GettingTheUsersTask_T_TheUsersTasksAreReturned()
		{
			ITaskService target = new TaskService(mockedContext.Object, userRepositoryMock, stateRepositoryMock, taskRepositoryMock);

			var tasks = target.GetTaskForUser("Martin");

			CollectionAssert.IsNotEmpty(tasks);
		}
	}
}