﻿using Gtd2.Domain.Entities;
using Gtd2.Domain.Interfaces;
using Gtd2.Infrastructure.Data.Mapping;
using Gtd2.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Test;
using System.Security.Cryptography;
using Gtd2.Infrastructure.Data.Repositories;
using Gtd2.Domain.Exceptions;

namespace Gtd2.Services
{
	[TestFixture]
	public class UserServiceTests
	{
		private User[] fakeUsers;
		string userName = "Martin", passWord = "abbc123";

		private Mock<Gtd2Context> mockedContext = new Mock<Gtd2Context>();
		private IUserRepository userRepositoryMock;

		[SetUp]
		public void SetUp()
		{
			RNGCryptoServiceProvider rngCryptoSP = new RNGCryptoServiceProvider();
			byte[] saltArr = new byte[10];

			rngCryptoSP.GetBytes(saltArr);
			var salt1 = Convert.ToBase64String(saltArr);
			var user = User.Create(userName, passWord, salt1);

			fakeUsers = new User[] 
 			{
				user
			};

			mockedContext.Setup(c => c.Users).ReturnsDbSet(fakeUsers);
			userRepositoryMock = new UserRepository(mockedContext.Object);			
		}

		[Test]
		public void G_ARealUser_W_Authenticting_T_ShouldReturnTrue()
		{
			IUserService target = new UserService(mockedContext.Object, userRepositoryMock);

			var result = target.AuthenticateUser(userName, passWord);

			Assert.IsTrue(result);
		}

		[Test]
		public void G_AFakeUser_W_Authenticting_T_ShouldReturnFalse()
		{
			IUserService target = new UserService(mockedContext.Object, userRepositoryMock);

			string fakeUserName = "lkenf", fakePassword = "sdfgs";

			var result = target.AuthenticateUser(fakeUserName, fakePassword);

			Assert.IsFalse(result);
		}

		[Test]
		public void G_ANewCorrectUser_W_RegisteringUser_T_ShouldNotThrowException()
		{
			IUserService target = new UserService(mockedContext.Object, userRepositoryMock);

			string newUserName = "sedfgsed", newPassword = "asfasa";
			string confirmPassword = newPassword;

			Assert.DoesNotThrow(() => target.RegisterUser(newUserName, newPassword, confirmPassword));
		}

		[Test]
		public void G_ANewUserWhereThePasswordsDoesntMatch_W_RegisteringUser_T_ShouldThrowRepeatPasswordException()
		{
			IUserService target = new UserService(mockedContext.Object, userRepositoryMock);

			string newUserName = "sedfgsed", newPassword = "asfasa";
			string confirmPassword = "ethrdxrfd";
			
			Assert.Throws(typeof(RepeatPasswordException), () => target.RegisterUser(newUserName, newPassword, confirmPassword));
		}

		[Test]
		public void G_AnExistingtUser_W_RegisteringUser_T_ShouldThrowUserAlreadyExistsException()
		{
			IUserService target = new UserService(mockedContext.Object, userRepositoryMock);
			
			/*
			var result = target.RegisterUser(userName, passWord, passWord);

			Assert.IsFalse(result);
			*/

			Assert.Throws(typeof(UserAlreadyExistsException), () => target.RegisterUser(userName, passWord, passWord));
		}


	}
}