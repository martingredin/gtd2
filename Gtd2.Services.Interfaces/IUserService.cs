﻿using Gtd2.Domain.Entities;

namespace Gtd2.Services.Interfaces
{
	public interface IUserService
	{
		bool AuthenticateUser(string userName, string password);
		void RegisterUser(string userName, string password, string confirmPassword);
		User GetUser(string userName);
	}
}
