﻿using Gtd2.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd2.Services.Interfaces
{
	public interface IStateService
	{
		KanbanState GetState(int stateId);

		IEnumerable<KanbanState> GetAllStates();
	}
}
